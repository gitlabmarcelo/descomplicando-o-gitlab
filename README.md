# Descomplicando o GitLab

Treinamento Descomplicando o GitLab criado ao vivo na Twitch


### Day-1

```bash
- Entendemos o que é o Git
- Enendemos o que é Working Dir, Index e HEAD
- Entendemos o que é o GitLab
- Como criar um Grupo no GitLab
- Como criar um repositório Git
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um Membro no projeto
- Como fazer o merge na Master/Main
```
